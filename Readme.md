# Setup instructions

- https://jupyter-cloud.gwdg.de
- Login
- Menu: Git -> Add Remote Repository
    or if not available
    Menu: Git -> Clone a repository
- https://gitlab.gwdg.de/mpi-dortmund/ze-edv-public/Computer_Tutorials
